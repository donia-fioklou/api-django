from django.urls import path, include
from rest_framework import routers
from learnApp.views.exercice import ExerciceViewSet
from learnApp.views.materiel import MaterielViewSet 
from learnApp.views.programme import ProgrammeViewSet
from learnApp.views.categorieExercice import CategorieExerciceViewSet
from learnApp.views.categorieProduit import CategorieProduitViewSet
from learnApp.views.produit import ProduitViewSet
router = routers.SimpleRouter()
router.register('programme', ProgrammeViewSet, basename='programme')
router.register('exercice', ExerciceViewSet, basename='exercice')
router.register('materiel', MaterielViewSet, basename='materiel')
router.register('categorie-exercice', CategorieExerciceViewSet, basename='categorie-exercice')
router.register('categorie-produit', CategorieProduitViewSet, basename='categorie-produit')
router.register('produit', ProduitViewSet, basename='produit')
urlpatterns = [
    path('', include(router.urls)),
   
]
