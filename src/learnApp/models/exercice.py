from django.db import models
from learnApp.models.categorieExercice import CategorieExercice

from learnApp.models.programme import Programme
class Exercice(models.Model):
    programme = models.ForeignKey(Programme, on_delete=models.CASCADE)
    categorieExercice = models.ForeignKey(CategorieExercice, on_delete=models.CASCADE)
    libelle = models.CharField(max_length=255)
    dureeConseille = models.IntegerField()
    repetitionConseille = models.IntegerField()
    serieConseille = models.CharField(max_length=255)
    
    def __str__(self):
        return self.libelle