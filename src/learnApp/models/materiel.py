from django.db import models

from learnApp.models.programme import Programme

class Materiel(models.Model):
    programme = models.ForeignKey(Programme, on_delete=models.CASCADE)
    libelle=models.CharField(max_length=255)