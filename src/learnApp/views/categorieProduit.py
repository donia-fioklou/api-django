from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from learnApp.models.categorieProduit import CategorieProduit
from learnApp.serializers.categorieProduit import CategorieProduitSerializer
 

class CategorieProduitViewSet(viewsets.ModelViewSet):
    queryset = CategorieProduit.objects.all()
    serializer_class = CategorieProduitSerializer  # Replace with the actual serializer for your model

    permission_classes = [IsAuthenticated]