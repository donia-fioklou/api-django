from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from learnApp.models.programme import Programme
from learnApp.serializers.programme import ProgrammeSerializer
 

class ProgrammeViewSet(viewsets.ModelViewSet):
    queryset = Programme.objects.all()
    serializer_class = ProgrammeSerializer  # Replace with the actual serializer for your model

    permission_classes = [IsAuthenticated]