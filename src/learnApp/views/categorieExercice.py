from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from learnApp.models.categorieExercice import CategorieExercice
from learnApp.serializers.categorieExercice import CategorieExerciceSerializer
 

class CategorieExerciceViewSet(viewsets.ModelViewSet):
    queryset = CategorieExercice.objects.all()
    serializer_class = CategorieExerciceSerializer  # Replace with the actual serializer for your model

    permission_classes = [IsAuthenticated]