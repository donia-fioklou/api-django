from rest_framework import serializers

from learnApp.models.materiel import Materiel


class MaterielSerializer(serializers.ModelSerializer):
    class Meta:
        model = Materiel
        fields = '__all__'
