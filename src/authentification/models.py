from django.db import models
from django.contrib.auth.models import  AbstractUser

from learnApp.models.programme import Programme
class TypeUtilisateur(models.Model):
    libelle = models.CharField(max_length=50)
    def __str__(self):
        return self.libelle
    
    
class User(AbstractUser):
    typeUtilisateur = models.ForeignKey(TypeUtilisateur,on_delete=models.CASCADE)
    programme = models.ForeignKey(Programme,on_delete=models.CASCADE)
    sexe = (('M','Masculin'),('F','Feminin'))
    telephone = models.CharField(max_length=50)
    dateNaissance = models.DateField(null=True,blank=True)
    sexe = models.CharField(max_length=1,choices=sexe)
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=50,unique=True,default='')