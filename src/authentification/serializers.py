
from rest_framework import serializers

from authentification.models import TypeUtilisateur, User
class TypeUtilisateurSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeUtilisateur
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = (
            'typeUtilisateur',
            'email',
            'last_name',
            'first_name',
            'sexe',
            'dateNaissance',
            'telephone',
            'password',
            'username',
            'programme',
            
        )
        extra_kwargs = {
            'password': {'write_only': True},
        }

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)
        return super(UserSerializer, self).update(instance, validated_data)