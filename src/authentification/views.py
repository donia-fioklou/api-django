from authentification.models import User, TypeUtilisateur
from authentification.serializers import TypeUtilisateurSerializer, UserSerializer
from rest_framework import viewsets

class TypeUtilisateur(viewsets.ModelViewSet):
    queryset = TypeUtilisateur.objects.all()
    serializer_class = TypeUtilisateurSerializer
    
class Utilisateur(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer